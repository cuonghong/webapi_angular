﻿(function (app) {
    'use strict';

    app.controller('productsCtrl', productsCtrl);

    productsCtrl.$inject = ['$scope', '$modal', 'apiService', 'notificationService'];

    function productsCtrl($scope, $modal, apiService, notificationService) {
        $scope.pageClass = 'page-products';
        $scope.loadingProducts = true;
        $scope.page = 0;
        $scope.pagesCount = 0;
        $scope.Products = [];
        $scope.selectedProducts = [];
        $scope.search = search;
        $scope.clearSearch = clearSearch;
        $scope.openDeletedDialog = openDeletedDialog;
        $scope.openMultipleDeletedDialog = openMultipleDeletedDialog;
        $scope.isDisabled = false;
        $scope.isCheckedSingleProduct = isChecked;
        $scope.synchronizeSingleProduct = synchronize;
        $scope.checkedAndSynchronizeAllProducts = checkedAndSynchronizeAllProducts;
        $scope.checkbox = false;
        $scope.getSearchUndefined = getSearchUndefined;

        function getSearchUndefined(valueSearch) {
            return (valueSearch === undefined || valueSearch === "");
        }

        function checkedAndSynchronizeAllProducts(products) {
            for (var i = 0; i < products.length; i++) {
                synchronize($scope.checkbox, products[i]);
            }
        }

        function isChecked(id) {
            var match = false;
            for (var i = 0 ; i < $scope.selectedProducts.length; i++) {
                if ($scope.selectedProducts[i].ID.toString() == id.toString()) {
                    match = true;
                }
            }
            return match;
        }

        function synchronize(bool, item) {
            if (bool) {
                // add item
                $scope.selectedProducts.push(item);

                // enable delete all button
                if ($scope.selectedProducts.length <= 1) {
                    $scope.isDisabled = true;
                }
            } else {
                // remove item
                for (var i = 0 ; i < $scope.selectedProducts.length; i++) {
                    if ($scope.selectedProducts[i].ID.toString() == item.ID.toString()) {
                        $scope.selectedProducts.splice(i, 1);
                    }
                }

                // disable delete all button
                if ($scope.selectedProducts.length <= 0) {
                    $scope.isDisabled = false;
                }
            }
        }

        function search(page) {
            $scope.valueSearch = $scope.filterProducts;
            debugger;
            page = page || 0;

            $scope.loadingProducts = true;

            var config = {
                params: {
                    page: page,
                    pageSize: 10,
                    filter: $scope.filterProducts
                }
            };

            apiService.get('/api/products/', config,
            productsLoadCompleted,
            productsLoadFailed);
        }

        function openDeletedDialog(product) {
            $scope.DeletedProduct = product;
            $modal.open({
                templateUrl: 'web/components/products/deletedProductModal.html',
                controller: 'productDeleteCtrl',
                scope: $scope
            }).result.then(function ($scope) {
                clearSearch();
            }, function () {
            });
        }

        function openMultipleDeletedDialog(data) {
            $scope.DeletedProducts = data;
            $modal.open({
                templateUrl: 'web/components/products/multipleDeletedProductModal.html',
                controller: 'productDeleteCtrl',
                scope: $scope
            }).result.then(function ($scope) {
                clearSearch();
            }, function () {
            });
        }

        function productsLoadCompleted(result) {
            $scope.Products = result.data.Items;
            $scope.page = result.data.Page;
            $scope.pagesCount = result.data.TotalPages;
            $scope.totalCount = result.data.TotalCount;
            $scope.loadingProducts = false;

            if ($scope.filterProducts && $scope.filterProducts.length) {
                notificationService.displayInfo(result.data.Items.length + ' products found');
            }

        }

        function productsLoadFailed(response) {
            notificationService.displayError(response.data);
        }

        function clearSearch() {
            $scope.filterProducts = '';
            search();
        }

        $scope.search();
    }

})(angular.module('WebApiAngular'));