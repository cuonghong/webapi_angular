﻿(function (app) {
    'use strict';

    app.controller('productEditCtrl', productEditCtrl);

    productEditCtrl.$inject = ['$scope', '$location', '$routeParams', 'apiService', 'notificationService', 'fileUploadService'];

    function productEditCtrl($scope, $location, $routeParams, apiService, notificationService, fileUploadService) {
        $scope.pageClass = 'page-products';
        $scope.product = {};
        $scope.categories = [];
        $scope.loadingProduct = true;
        $scope.isReadOnly = false;
        $scope.UpdateProduct = UpdateProduct;
        $scope.prepareFiles = prepareFiles;
        $scope.openDatePicker = openDatePicker;

        $scope.dateOptions = {
            formatYear: 'yy',
            startingDay: 1
        };
        $scope.datepicker = {};

        var productImage = null;

        function loadProduct() {

            $scope.loadingProduct = true;

            apiService.get('/api/products/detail/' + $routeParams.id, null,
            productLoadCompleted,
            productLoadFailed);
        }

        function productLoadCompleted(result) {
            $scope.product = result.data;
            $scope.loadingProduct = false;

            loadCategories();
        }

        function productLoadFailed(response) {
            notificationService.displayError(response.data);
        }

        function categoriesLoadCompleted(response) {
            $scope.categories = response.data;
        }

        function categoriesLoadFailed(response) {
            notificationService.displayError(response.data);
        }

        function loadCategories() {
            apiService.get('/api/categories/', null,
            categoriesLoadCompleted,
            categoriesLoadFailed);
        }

        function UpdateProduct() {
            if (productImage) {
                fileUploadService.uploadImage(productImage, $scope.product.ID, UpdateProductModel, "products");
            }
            else
                UpdateProductModel();
        }

        function UpdateProductModel() {
            apiService.post('/api/products/update', $scope.product,
            updateProductSucceded,
            updateProductFailed);
        }

        function prepareFiles($files) {
            productImage = $files;
        }

        function updateProductSucceded(response) {
            console.log(response);
            notificationService.displaySuccess($scope.product.Title + ' has been updated');
            $scope.product = response.data;
            productImage = null;
            redirectToIndex();
        }

        function redirectToIndex() {
            $location.url('products/list');
        }

        function updateProductFailed(response) {
            notificationService.displayError(response);
        }

        function openDatePicker($event) {
            $event.preventDefault();
            $event.stopPropagation();

            $scope.datepicker.opened = true;
        };

        loadProduct();
    }

})(angular.module('WebApiAngular'));