﻿(function (app) {
    'use strict';

    app.controller('productAddCtrl', productAddCtrl);

    productAddCtrl.$inject = ['$scope', '$location', '$routeParams', 'apiService', 'notificationService', 'fileUploadService'];

    function productAddCtrl($scope, $location, $routeParams, apiService, notificationService, fileUploadService) {

        $scope.pageClass = 'page-products';
        $scope.product = { CategoryId: 1, Rating: 1 };

        $scope.categories = [];
        $scope.isReadOnly = false;
        $scope.AddProduct = AddProduct;
        $scope.prepareFiles = prepareFiles;
        $scope.openDatePicker = openDatePicker;

        $scope.dateOptions = {
            formatYear: 'yy',
            startingDay: 1
        };
        $scope.datepicker = {};

        var productImage = null;

        function loadCategories() {
            apiService.get('/api/categories/', null,
            categoriesLoadCompleted,
            categoriesLoadFailed);
        }

        function categoriesLoadCompleted(response) {
            $scope.categories = response.data;
        }

        function categoriesLoadFailed(response) {
            notificationService.displayError(response.data);
        }

        function AddProduct() {
            AddProductModel();
        }

        function AddProductModel() {
            apiService.post('/api/products/add', $scope.product,
            addProductSucceded,
            addProductFailed);
        }

        function prepareFiles($files) {
            productImage = $files;
        }

        function addProductSucceded(response) {
            notificationService.displaySuccess($scope.product.Title + ' has been submitted to Home Cinema');
            $scope.product = response.data;

            if (productImage) {
                fileUploadService.uploadImage(productImage, $scope.product.ID, redirectToIndex, "products");
            }
            else
                redirectToIndex();
        }

        function addProductFailed(response) {
            console.log(response);
            notificationService.displayError(response.statusText);
        }

        function openDatePicker($event) {
            $event.preventDefault();
            $event.stopPropagation();

            $scope.datepicker.opened = true;
        };

        function redirectToIndex() {
            $location.url('products/list');
        }

        loadCategories();
    }

})(angular.module('WebApiAngular'));