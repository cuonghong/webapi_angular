﻿(function (app) {
    'use strict';

    app.controller('productDeleteCtrl', productDeleteCtrl);

    productDeleteCtrl.$inject = ['$scope', '$location', '$modalInstance', '$timeout', 'apiService', 'notificationService', '$route'];

    function productDeleteCtrl($scope, $location, $modalInstance, $timeout, apiService, notificationService, $route) {

        $scope.cancelDelete = cancelDelete;
        $scope.deleteProduct = deleteProduct;
        $scope.deleteMultipleProducts = deleteMultipleProducts;

        function deleteProduct() {
            console.log($scope.DeletedProduct);
            apiService.post('/api/products/delete/', $scope.DeletedProduct,
            deletedProductCompleted,
            deletedProductLoadFailed);
        }

        function deleteMultipleProducts() {
            console.log($scope.DeletedProducts);
            apiService.post('/api/products/multipledelete/', $scope.DeletedProducts,
            deletedMultipleProductsCompleted,
            deletedMultipleProductsLoadFailed);
        }

        function deletedProductCompleted(response) {
            notificationService.displaySuccess($scope.DeletedProduct.Title + ' has been deleted');
            $scope.DeletedProduct = {};
            $modalInstance.dismiss();
            redirectToIndex();
        }

        function deletedMultipleProductsCompleted(response) {
            notificationService.displaySuccess('Multiple products has been deleted');
            $scope.DeletedProduct = {};
            $modalInstance.dismiss();
            redirectToIndex();
        }

        function redirectToIndex() {
            $route.reload();
        }

        function deletedProductLoadFailed(response) {
            notificationService.displayError(response.data);
        }

        function deletedMultipleProductsLoadFailed(response) {
            notificationService.displayError(response.data);
        }

        function cancelDelete() {
            $scope.isEnabled = false;
            $modalInstance.dismiss();
        }
    }

})(angular.module('WebApiAngular'));