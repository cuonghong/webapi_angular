﻿using FluentValidation;
using WebApiAngular.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiAngular.Web.Infrastructure.Validators
{
    public class ProductViewModelValidator : AbstractValidator<ProductViewModel>
    {
        public ProductViewModelValidator()
        {
            RuleFor(product => product.CategoryId).GreaterThan(0)
                .WithMessage("Select a Genre");

            RuleFor(product => product.Director).NotEmpty().Length(1,100)
                .WithMessage("Select a Director");

            RuleFor(product => product.Writer).NotEmpty().Length(1,50)
                .WithMessage("Select a writer");

            RuleFor(product => product.Producer).NotEmpty().Length(1, 50)
                .WithMessage("Select a producer");

            RuleFor(product => product.Description).NotEmpty()
                .WithMessage("Select a description");

            RuleFor(product => product.Rating).InclusiveBetween((byte)0, (byte)5)
                .WithMessage("Rating must be less than or equal to 5");

            RuleFor(product => product.TrailerURI).NotEmpty().Must(ValidTrailerURI)
                .WithMessage("Only Youtube Trailers are supported");
        }

        private bool ValidTrailerURI(string trailerURI)
        {
            return (!string.IsNullOrEmpty(trailerURI) && trailerURI.ToLower().StartsWith("https://www.youtube.com/watch?"));
        }
    }
}