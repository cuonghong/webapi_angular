﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace WebApiAngular.Web
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
               name: "login",
               url: "login",
               defaults: new { controller = "Commerce", action = "Admin", id = UrlParameter.Optional }
           );

            routes.MapRoute(
               name: "list",
               url: "products/list",
               defaults: new { controller = "Commerce", action = "Admin", id = UrlParameter.Optional }
           );

            routes.MapRoute(
               name: "add",
               url: "products/add",
               defaults: new { controller = "Commerce", action = "Admin", id = UrlParameter.Optional }
           );

            routes.MapRoute(
               name: "edit",
               url: "products/edit/{id}",
               defaults: new { controller = "Commerce", action = "Admin", id = UrlParameter.Optional }
           );

            routes.MapRoute(
               name: "delete",
               url: "products/delete/{id}",
               defaults: new { controller = "Commerce", action = "Admin", id = UrlParameter.Optional }
           );

            routes.MapRoute(
              name: "public",
              url: "products",
              defaults: new { controller = "Commerce", action = "Public", id = UrlParameter.Optional }
          );

            routes.MapMvcAttributeRoutes();

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Commerce", action = "Public", id = UrlParameter.Optional }
            );

        }
    }
}
