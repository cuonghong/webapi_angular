﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebApiAngular.Data.Infrastructure
{
    public class DbFactory : Disposable, IDbFactory
    {
        WebApiAngularContext dbContext;

        public WebApiAngularContext Init()
        {
            return dbContext ?? (dbContext = new WebApiAngularContext());
        }

        protected override void DisposeCore()
        {
            if (dbContext != null)
                dbContext.Dispose();
        }
    }
}
