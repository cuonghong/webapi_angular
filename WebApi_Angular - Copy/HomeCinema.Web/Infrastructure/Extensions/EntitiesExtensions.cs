﻿using WebApiAngular.Entities;
using WebApiAngular.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiAngular.Web.Infrastructure.Extensions
{
    public static class EntitiesExtensions
    {
       
        public static void UpdateProduct(this Product movie, ProductViewModel productVm)
        {
            movie.Title = productVm.Title;
            movie.Description = productVm.Description;
            movie.CategoryId = productVm.CategoryId;
            movie.Director = productVm.Director;
            movie.Writer = productVm.Writer;
            movie.Producer = productVm.Producer;
            movie.Rating = productVm.Rating;
            movie.TrailerURI = productVm.TrailerURI;
            movie.ReleaseDate = productVm.ReleaseDate;
        }

        public static void UpdateCustomer(this Customer customer, CustomerViewModel customerVm)
        {
            customer.FirstName = customerVm.FirstName;
            customer.LastName = customerVm.LastName;
            customer.IdentityCard = customerVm.IdentityCard;
            customer.Mobile = customerVm.Mobile;
            customer.DateOfBirth = customerVm.DateOfBirth;
            customer.Email = customerVm.Email;
            customer.UniqueKey = (customerVm.UniqueKey == null || customerVm.UniqueKey == Guid.Empty)
                ? Guid.NewGuid() : customerVm.UniqueKey;
            customer.RegistrationDate = (customer.RegistrationDate == DateTime.MinValue ? DateTime.Now : customerVm.RegistrationDate);
        }
    }
}