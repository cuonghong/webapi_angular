﻿using AutoMapper;
using WebApiAngular.Entities;
using WebApiAngular.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiAngular.Web.Mappings
{
    public class DomainToViewModelMappingProfile : Profile
    {
        public override string ProfileName
        {
            get { return "DomainToViewModelMappings"; }
        }

        protected override void Configure()
        {
           Mapper.CreateMap<Product, ProductViewModel>()
               .ForMember(vm => vm.Category, map => map.MapFrom(m => m.Category.Name))
               .ForMember(vm => vm.CategoryId, map => map.MapFrom(m => m.Category.ID))
               .ForMember(vm => vm.Image, map => map.MapFrom(m => string.IsNullOrEmpty(m.Image) == true ? "no-image.gif" : m.Image));
           Mapper.CreateMap<Category, CategoryViewModel>()
                .ForMember(vm => vm.NumberOfProducts, map => map.MapFrom(g => g.Products.Count()));

            // code omitted
            Mapper.CreateMap<Customer, CustomerViewModel>();
        }
    }
}