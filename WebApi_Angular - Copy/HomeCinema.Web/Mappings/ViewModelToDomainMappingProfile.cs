﻿using AutoMapper;
using WebApiAngular.Entities;
using WebApiAngular.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiAngular.Web.Mappings
{
    public class ViewModelToDomainMappingProfile : Profile
    {
        public override string ProfileName
        {
            get { return "ViewModelToDomainMappings"; }
        }

        protected override void Configure()
        {

            Mapper.CreateMap<ProductViewModel, Product>()
                //.ForMember(m => m.Image, map => map.Ignore())
                .ForMember(m => m.Category, map => map.Ignore());
        }
    }
}