﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;

namespace WebApiAngular.Web.App_Start
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/libraries").Include(
                "~/Web/libraries/jquery.js",
                "~/Web/libraries/toastr.js",
                "~/Web/libraries/angular.js",
                "~/Web/libraries/angular-route.js",
                "~/Web/libraries/angular-cookies.js",
                "~/Web/libraries/angular-validator.js",
                "~/Web/libraries/angular-base64.js",
                "~/Web/libraries/angular-file-upload.js",
                "~/Web/libraries/angucomplete-alt.min.js",
                "~/Web/libraries/ui-bootstrap-tpls-0.13.1.js",
                "~/Web/libraries/loading-bar.js"
                ));

            bundles.Add(new ScriptBundle("~/bundles/components").Include(
                "~/Web/components/modules/common.core.js",
                "~/Web/components/modules/common.ui.js",
                "~/Web/components/app.js",
                "~/Web/components/services/apiService.js",
                "~/Web/components/services/notificationService.js",
                "~/Web/components/services/membershipService.js",
                "~/Web/components/services/fileUploadService.js",
                "~/Web/components/layout/topBar.directive.js",
                "~/Web/components/layout/topMenu.directive.js",
                "~/Web/components/layout/sideBar.directive.js",
                "~/Web/components/layout/sideMenu.directive.js",
                "~/Web/components/layout/customPager.directive.js",
                "~/Web/components/directives/rating.directive.js",
                "~/Web/components/directives/availableMovie.directive.js",
                "~/Web/components/directives/checkImage.directive.js",
                "~/Web/components/user/loginCtrl.js",
                "~/Web/components/user/registerCtrl.js",
                "~/Web/components/home/adminCtrl.js",
                "~/Web/components/home/publicCtrl.js",
                "~/Web/components/home/indexCtrl.js",
                "~/Web/components/customers/customersCtrl.js",
                "~/Web/components/customers/customersRegCtrl.js",
                "~/Web/components/customers/customerEditCtrl.js",
                "~/Web/components/movies/moviesCtrl.js",
                "~/Web/components/movies/movieAddCtrl.js",
                "~/Web/components/movies/movieDetailsCtrl.js",
                "~/Web/components/movies/movieEditCtrl.js",

                "~/Web/components/products/productsCtrl.js",
                "~/Web/components/products/productAddCtrl.js",
                "~/Web/components/products/productDetailCtrl.js",
                "~/Web/components/products/productEditCtrl.js",
                "~/Web/components/products/productDeleteCtrl.js",

                "~/Web/components/controllers/rentalCtrl.js",
                "~/Web/components/rental/rentMovieCtrl.js",
                "~/Web/components/rental/rentStatsCtrl.js"
                ));

            bundles.Add(new StyleBundle("~/Content/css").Include(
               "~/content/css/toastr.css",
               "~/content/css/jquery.fancybox.css",
               "~/content/css/loading-bar.css"));

            BundleTable.EnableOptimizations = false;
        }
    }
}