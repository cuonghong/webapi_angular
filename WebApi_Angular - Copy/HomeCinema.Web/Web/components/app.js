﻿(function () {
    'use strict';

    angular.module('WebApiAngular', ['common.core', 'common.ui'])
        .config(config)
        .run(run);

    config.$inject = ['$routeProvider', '$locationProvider'];
    function config($routeProvider, $locationProvider) {
        $routeProvider
            .when("/", {
                templateUrl: "web/components/home/index.html",
                controller: "indexCtrl"
            })
            .when("/login", {
                templateUrl: "web/components/user/login.html",
                controller: "loginCtrl"
            })
            .when("/register", {
                templateUrl: "web/components/user/register.html",
                controller: "registerCtrl"
            })
            .when("/customers/list", {
                templateUrl: "web/components/customers/customers.html",
                controller: "customersCtrl"
            })
            .when("/customers/register", {
                templateUrl: "web/components/customers/register.html",
                controller: "customersRegCtrl",
                resolve: { isAuthenticated: isAuthenticated }
            })
            .when("/movies", {
                templateUrl: "web/components/movies/movies.html",
                controller: "moviesCtrl"
            })
            .when("/movies/add", {
                templateUrl: "web/components/movies/add.html",
                controller: "movieAddCtrl",
                resolve: { isAuthenticated: isAuthenticated }
            })
            .when("/movies/:id", {
                templateUrl: "web/components/movies/details.html",
                controller: "movieDetailsCtrl",
                resolve: { isAuthenticated: isAuthenticated }
            })
            .when("/movies/edit/:id", {
                templateUrl: "web/components/movies/edit.html",
                controller: "movieEditCtrl"
            })


            .when("/products/list", {
                templateUrl: "web/components/products/products.html",
                controller: "productsCtrl"
            })
            .when("/products/add", {
                templateUrl: "web/components/products/add.html",
                controller: "productAddCtrl",
                resolve: { isAuthenticated: isAuthenticated }
            })
            .when("/products/edit/:id", {
                templateUrl: "web/components/products/edit.html",
                controller: "productEditCtrl"
            })
            .when("/products/detail/:id", {
                templateUrl: "web/components/products/detail.html",
                controller: "productDetailCtrl",
                resolve: { isAuthenticated: isAuthenticated }
            })

            .when("/products/delete/:id", {
                templateUrl: "web/components/products/confirmDelete.html",
                controller: "productDeleteCtrl",
                resolve: { isAuthenticated: isAuthenticated }
            })

            .when("/rental", {
                templateUrl: "web/components/rental/rental.html",
                controller: "rentStatsCtrl"
            }).otherwise({ redirectTo: "/" });

        $locationProvider.html5Mode(true);
    }

    run.$inject = ['$rootScope', '$location', '$cookieStore', '$http'];

    function run($rootScope, $location, $cookieStore, $http) {
        // handle page refreshes
        $rootScope.repository = $cookieStore.get('repository') || {};
        if ($rootScope.repository.loggedUser) {
            $http.defaults.headers.common['Authorization'] = $rootScope.repository.loggedUser.authdata;
        }
    }

    isAuthenticated.$inject = ['membershipService', '$rootScope', '$location'];

    function isAuthenticated(membershipService, $rootScope, $location) {
        if (!membershipService.isUserLoggedIn()) {
            $rootScope.previousState = $location.path();
            $location.path('/login');
        }
    }

})();