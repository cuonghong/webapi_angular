﻿(function(app) {
    'use strict';

    app.directive('sideMenu', sideMenu);

    function sideMenu() {
        return {
            restrict: 'E',
            replace: true,
            templateUrl: '/web/components/layout/sideMenu.html'
        }
    }

})(angular.module('common.ui'));