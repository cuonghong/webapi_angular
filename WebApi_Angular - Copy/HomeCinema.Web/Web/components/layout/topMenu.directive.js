﻿(function(app) {
    'use strict';

    app.directive('topMenu', topMenu);

    function topMenu() {
        return {
            restrict: 'E',
            replace: true,
            templateUrl: '/web/components/layout/topMenu.html'
        }
    }

})(angular.module('common.ui'));