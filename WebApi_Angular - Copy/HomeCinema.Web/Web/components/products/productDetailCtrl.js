﻿(function (app) {
    'use strict';

    app.controller('productDetailCtrl', productDetailCtrl);

    productDetailCtrl.$inject = ['$scope', '$location', '$routeParams', '$modal', 'apiService', 'notificationService'];

    function productDetailCtrl($scope, $location, $routeParams, $modal, apiService, notificationService) {
        $scope.pageClass = 'page-products';
        $scope.product = {};
        $scope.loadingProduct = true;
        $scope.isReadOnly = true;
        $scope.getStatusColor = getStatusColor;

        function loadProduct() {

            $scope.loadingProduct = true;

            apiService.get('/api/products/detail/' + $routeParams.id, null,
            productLoadCompleted,
            productLoadFailed);
        }

        function loadProductDetail() {
            loadProduct();
        }

        function getStatusColor(status) {
            if (status == 'Borrowed')
                return 'red'
            else {
                return 'green';
            }
        }

        function productLoadCompleted(result) {
            $scope.product = result.data;
            $scope.loadingProduct = false;
        }

        function productLoadFailed(response) {
            notificationService.displayError(response.data);
        }

        loadProductDetail();
    }

})(angular.module('WebApiAngular'));