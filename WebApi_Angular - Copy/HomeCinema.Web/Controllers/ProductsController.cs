﻿using AutoMapper;
using WebApiAngular.Data.Infrastructure;
using WebApiAngular.Data.Repositories;
using WebApiAngular.Entities;
using WebApiAngular.Web.Infrastructure.Core;
using WebApiAngular.Web.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using WebApiAngular.Web.Infrastructure.Extensions;
using WebApiAngular.Data.Extensions;

namespace WebApiAngular.Web.Controllers
{
    [Authorize(Roles = "Admin")]
    [RoutePrefix("api/products")]
    public class ProductsController : ApiControllerBase
    {
        private readonly IEntityBaseRepository<Product> _productsRepository;

        public ProductsController(IEntityBaseRepository<Product> productsRepository,
            IEntityBaseRepository<Error> _errorsRepository, IUnitOfWork _unitOfWork)
            : base(_errorsRepository, _unitOfWork)
        {
            _productsRepository = productsRepository;
        }

        [AllowAnonymous]
        [Route("latest")]
        public HttpResponseMessage Get(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var products = _productsRepository.GetAll().OrderByDescending(m => m.ReleaseDate).Take(6).ToList();

                IEnumerable<ProductViewModel> productsVM = Mapper.Map<IEnumerable<Product>, IEnumerable<ProductViewModel>>(products);

                response = request.CreateResponse<IEnumerable<ProductViewModel>>(HttpStatusCode.OK, productsVM);

                return response;
            });
        }

        [Route("detail/{id:int}")]
        public HttpResponseMessage Get(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var product = _productsRepository.GetSingle(id);

                ProductViewModel productVM = Mapper.Map<Product, ProductViewModel>(product);

                response = request.CreateResponse<ProductViewModel>(HttpStatusCode.OK, productVM);

                return response;
            });
        }

        [AllowAnonymous]
        [Route("{page:int=0}/{pageSize=3}/{filter?}")]
        public HttpResponseMessage Get(HttpRequestMessage request, int? page, int? pageSize, string filter = null)
        {
            int currentPage = page.Value;
            int currentPageSize = pageSize.Value;

            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                List<Product> products = null;
                int totalProducts = new int();

                if (!string.IsNullOrEmpty(filter))
                {
                    products = _productsRepository
                        .FindBy(m => m.Title.ToLower()
                        .Contains(filter.ToLower().Trim()))
                        .OrderByDescending(m => m.ID)
                        .Skip(currentPage * currentPageSize)
                        .Take(currentPageSize)
                        .ToList();

                    totalProducts = _productsRepository
                        .FindBy(m => m.Title.ToLower()
                        .Contains(filter.ToLower().Trim()))
                        .Count();
                }
                else
                {
                    products = _productsRepository
                        .GetAll()
                        .OrderByDescending(m => m.ID)
                        .Skip(currentPage * currentPageSize)
                        .Take(currentPageSize)
                        .ToList();

                    totalProducts = _productsRepository.GetAll().Count();
                }

                IEnumerable<ProductViewModel> productsVM = Mapper.Map<IEnumerable<Product>, IEnumerable<ProductViewModel>>(products);

                PaginationSet<ProductViewModel> pagedSet = new PaginationSet<ProductViewModel>()
                {
                    Page = currentPage,
                    TotalCount = totalProducts,
                    TotalPages = (int)Math.Ceiling((decimal)totalProducts / currentPageSize),
                    Items = productsVM
                };

                response = request.CreateResponse<PaginationSet<ProductViewModel>>(HttpStatusCode.OK, pagedSet);

                return response;
            });
        }

        [HttpPost]
        [Route("add")]
        public HttpResponseMessage Add(HttpRequestMessage request, ProductViewModel product)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;

                if (!ModelState.IsValid)
                {
                    response = request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    Product newProduct = new Product();
                    newProduct.UpdateProduct(product);

                    _productsRepository.Add(newProduct);

                    _unitOfWork.Commit();

                    // Update view model
                    product = Mapper.Map<Product, ProductViewModel>(newProduct);
                    response = request.CreateResponse<ProductViewModel>(HttpStatusCode.Created, product);
                }

                return response;
            });
        }

        [HttpPost]
        [Route("update")]
        public HttpResponseMessage Update(HttpRequestMessage request, ProductViewModel product)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;

                if (!ModelState.IsValid)
                {
                    response = request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var productDb = _productsRepository.GetSingle(product.ID);
                    if (productDb == null)
                        response = request.CreateErrorResponse(HttpStatusCode.NotFound, "Invalid product.");
                    else
                    {
                        productDb.UpdateProduct(product);
                        product.Image = productDb.Image;
                        _productsRepository.Edit(productDb);

                        _unitOfWork.Commit();
                        response = request.CreateResponse<ProductViewModel>(HttpStatusCode.OK, product);
                    }
                }

                return response;
            });
        }

        [HttpPost]
        [Route("delete")]
        public HttpResponseMessage Delete(HttpRequestMessage request, ProductViewModel product)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;

                if (!ModelState.IsValid)
                {
                    response = request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    Product _product = _productsRepository.GetSingle(product.ID);
                    if (_product == null)
                        response = request.CreateErrorResponse(HttpStatusCode.NotFound, "Invalid product.");
                    else
                    {
                        _productsRepository.Delete(_product);

                        _unitOfWork.Commit();

                        // Remove image of product on server
                        RemoveImage(_product.Image);

                        response = request.CreateResponse<Product>(HttpStatusCode.OK, _product);
                    }
                }

                return response;
            });
        }

        [HttpPost]
        [Route("multipledelete")]
        public HttpResponseMessage MultipleDelete(HttpRequestMessage request, List<ProductViewModel> products)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;

                if (!ModelState.IsValid)
                {
                    response = request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    foreach (var product in products)
                    {
                        Product _product = _productsRepository.GetSingle(product.ID);
                        if (_product == null)
                            response = request.CreateErrorResponse(HttpStatusCode.NotFound, "Invalid product.");
                        else
                        {
                            _productsRepository.Delete(_product);

                            // Remove image of product on server
                            RemoveImage(_product.Image);

                            response = request.CreateResponse<Product>(HttpStatusCode.OK, _product);
                        }
                    }
                    _unitOfWork.Commit();
                }

                return response;
            });
        }

        [MimeMultipart]
        [Route("images/upload")]
        public HttpResponseMessage Post(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;

                var productOld = _productsRepository.GetSingle(id);
                if (productOld == null)
                    response = request.CreateErrorResponse(HttpStatusCode.NotFound, "Invalid product.");
                else
                {
                    var filePath = "/web/upload/images/products";
                    var uploadPath = HttpContext.Current.Server.MapPath(filePath);

                    var multipartFormDataStreamProvider = new UploadMultipartFormProvider(uploadPath);

                    // Read the MIME multipart asynchronously 
                    Request.Content.ReadAsMultipartAsync(multipartFormDataStreamProvider);

                    string _localFileName = multipartFormDataStreamProvider
                        .FileData.Select(multiPartData => multiPartData.LocalFileName).FirstOrDefault();

                    // Remove old image
                    RemoveImage(productOld.Image);

                    // Create response
                    FileUploadResult fileUploadResult = new FileUploadResult
                    {
                        LocalFilePath = _localFileName,

                        FileName = Path.GetFileName(_localFileName),

                        FileLength = new FileInfo(_localFileName).Length
                    };

                    // Update database
                    productOld.Image = fileUploadResult.FileName;
                    _productsRepository.Edit(productOld);
                    _unitOfWork.Commit();

                    response = request.CreateResponse(HttpStatusCode.OK, fileUploadResult);
                }

                return response;
            });
        }

        [MimeMultipart]
        [Route("images/remove")]
        public bool RemoveImage(string imageName)
        {
            var filePath = HttpContext.Current.Server.MapPath("/web/upload/images/products/" + imageName);

            if (File.Exists(filePath))
            {
                File.Delete(filePath);
            }

            return true;
        }
    }
}