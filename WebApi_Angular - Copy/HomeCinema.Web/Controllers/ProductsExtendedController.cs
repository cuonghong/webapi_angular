﻿using AutoMapper;
using WebApiAngular.Data.Infrastructure;
using WebApiAngular.Data.Repositories;
using WebApiAngular.Entities;
using WebApiAngular.Web.Infrastructure.Core;
using WebApiAngular.Web.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using WebApiAngular.Web.Infrastructure.Extensions;
using WebApiAngular.Data.Extensions;

namespace WebApiAngular.Web.Controllers
{
    [Authorize(Roles = "Admin")]
    [RoutePrefix("api/productsextended")]
    public class ProductsExtendedController : ApiControllerBaseExtended
    {
        public ProductsExtendedController(IDataRepositoryFactory dataRepositoryFactory, IUnitOfWork unitOfWork)
            : base(dataRepositoryFactory, unitOfWork) { }

        [AllowAnonymous]
        [Route("latest")]
        public HttpResponseMessage Get(HttpRequestMessage request)
        {
            _requiredRepositories = new List<Type>() { typeof(Product) };

            return CreateHttpResponse(request, _requiredRepositories, () =>
            {
                HttpResponseMessage response = null;
                var products = _productsRepository.GetAll().OrderByDescending(m => m.ReleaseDate).Take(6).ToList();

                IEnumerable<ProductViewModel> productsVM = Mapper.Map<IEnumerable<Product>, IEnumerable<ProductViewModel>>(products);

                response = request.CreateResponse<IEnumerable<ProductViewModel>>(HttpStatusCode.OK, productsVM);

                return response;
            });
        }

        [Route("detail/{id:int}")]
        public HttpResponseMessage Get(HttpRequestMessage request, int id)
        {
            _requiredRepositories = new List<Type>() { typeof(Product) };

            return CreateHttpResponse(request, _requiredRepositories, () =>
            {
                HttpResponseMessage response = null;
                var product = _productsRepository.GetSingle(id);

                ProductViewModel productVM = Mapper.Map<Product, ProductViewModel>(product);

                response = request.CreateResponse<ProductViewModel>(HttpStatusCode.OK, productVM);

                return response;
            });
        }

        [AllowAnonymous]
        [Route("{page:int=0}/{pageSize=3}/{filter?}")]
        public HttpResponseMessage Get(HttpRequestMessage request, int? page, int? pageSize, string filter = null)
        {
            _requiredRepositories = new List<Type>() { typeof(Product) };
            int currentPage = page.Value;
            int currentPageSize = pageSize.Value;

            return CreateHttpResponse(request, _requiredRepositories, () =>
            {
                HttpResponseMessage response = null;
                List<Product> products = null;
                int totalProducts = new int();

                if (!string.IsNullOrEmpty(filter))
                {
                    products = _productsRepository.GetAll()
                        .OrderBy(m => m.ID)
                        .Where(m => m.Title.ToLower()
                        .Contains(filter.ToLower().Trim()))
                        .ToList();
                }
                else
                {
                    products = _productsRepository.GetAll().ToList();
                }

                totalProducts = products.Count();
                products = products.Skip(currentPage * currentPageSize)
                        .Take(currentPageSize)
                        .ToList();

                IEnumerable<ProductViewModel> productsVM = Mapper.Map<IEnumerable<Product>, IEnumerable<ProductViewModel>>(products);

                PaginationSet<ProductViewModel> pagedSet = new PaginationSet<ProductViewModel>()
                {
                    Page = currentPage,
                    TotalCount = totalProducts,
                    TotalPages = (int)Math.Ceiling((decimal)totalProducts / currentPageSize),
                    Items = productsVM
                };

                response = request.CreateResponse<PaginationSet<ProductViewModel>>(HttpStatusCode.OK, pagedSet);

                return response;
            });
        }

        [HttpPost]
        [Route("add")]
        public HttpResponseMessage Add(HttpRequestMessage request, ProductViewModel product)
        {
           

            return CreateHttpResponse(request, _requiredRepositories, () =>
            {
                HttpResponseMessage response = null;

                if (!ModelState.IsValid)
                {
                    response = request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    Product newProduct = new Product();
                    newProduct.UpdateProduct(product);


                    _productsRepository.Add(newProduct);

                    _unitOfWork.Commit();

                    // Update view model
                    product = Mapper.Map<Product, ProductViewModel>(newProduct);
                    response = request.CreateResponse<ProductViewModel>(HttpStatusCode.Created, product);
                }

                return response;
            });
        }

        [HttpPost]
        [Route("update")]
        public HttpResponseMessage Update(HttpRequestMessage request, ProductViewModel product)
        {
            _requiredRepositories = new List<Type>() { typeof(Product) };

            return CreateHttpResponse(request, _requiredRepositories, () =>
            {
                HttpResponseMessage response = null;

                if (!ModelState.IsValid)
                {
                    response = request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var productDb = _productsRepository.GetSingle(product.ID);
                    if (productDb == null)
                        response = request.CreateErrorResponse(HttpStatusCode.NotFound, "Invalid product.");
                    else
                    {
                        productDb.UpdateProduct(product);
                        product.Image = productDb.Image;
                        _productsRepository.Edit(productDb);

                        _unitOfWork.Commit();
                        response = request.CreateResponse<ProductViewModel>(HttpStatusCode.OK, product);
                    }
                }

                return response;
            });
        }

        [MimeMultipart]
        [Route("images/upload")]
        public HttpResponseMessage Post(HttpRequestMessage request, int productId)
        {
            _requiredRepositories = new List<Type>() { typeof(Product) };

            return CreateHttpResponse(request, _requiredRepositories, () =>
            {
                HttpResponseMessage response = null;

                var productOld = _productsRepository.GetSingle(productId);
                if (productOld == null)
                    response = request.CreateErrorResponse(HttpStatusCode.NotFound, "Invalid product.");
                else
                {
                    var uploadPath = HttpContext.Current.Server.MapPath("/web/upload/images/products");

                    var multipartFormDataStreamProvider = new UploadMultipartFormProvider(uploadPath);

                    // Read the MIME multipart asynchronously 
                    Request.Content.ReadAsMultipartAsync(multipartFormDataStreamProvider);

                    string _localFileName = multipartFormDataStreamProvider
                        .FileData.Select(multiPartData => multiPartData.LocalFileName).FirstOrDefault();

                    // Create response
                    FileUploadResult fileUploadResult = new FileUploadResult
                    {
                        LocalFilePath = _localFileName,

                        FileName = Path.GetFileName(_localFileName),

                        FileLength = new FileInfo(_localFileName).Length
                    };

                    // update database
                    productOld.Image = fileUploadResult.FileName;
                    _productsRepository.Edit(productOld);
                    _unitOfWork.Commit();

                    response = request.CreateResponse(HttpStatusCode.OK, fileUploadResult);
                }

                return response;
            });
        }
    }
}
